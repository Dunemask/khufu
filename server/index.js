const express = require("express");
const { createProxyMiddleware: proxy } = require("http-proxy-middleware");
const port = process.env.KHUFU_DEV_PORT ?? 52002;
const { REACT_APP_CAIRO_URL, REACT_APP_NUBIAN_URL } = process.env;
const cairoProxyOpt = {
  target: REACT_APP_CAIRO_URL,
  changeOrigin: true,
  pathRewrite: { "^/api/cairo": "/api" },
};
const nubianProxyOpt = {
  target: REACT_APP_NUBIAN_URL,
  changeOrigin: true,
  pathRewrite: { "^/api/nubian": "/api" },
};
const app = express();
app.use("/", express.static("build"));
app.use("/api/cairo", proxy(cairoProxyOpt));
app.use("/api/nubian", proxy(nubianProxyOpt));
app.get("/healthcheck", (req, res) => res.sendStatus(200));
app.get("/alive", (req, res) => res.sendStatus(200));
app.listen(port, () => console.log(`🌴 Nile px server started on ${port}! 🌴`));
