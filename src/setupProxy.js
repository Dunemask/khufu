const { createProxyMiddleware } = require("http-proxy-middleware");
const cairoUrl = process.env.CAIRO_PROXY_URL ?? "http://localhost:52002";
const nubianUrl = process.env.NUBIAN_PROXY_URL ?? "http://localhost:52002";
module.exports = (app) => {
  // Cairo Proxy
  app.use("/api/cairo", createProxyMiddleware({ target: cairoUrl }));
  app.use("/api/nubian", createProxyMiddleware({ target: nubianUrl }));
};
