// Module Imports
import React from "react";
import axios from "axios";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
// Local Imports
import StashBoard from "./stash/StashBoard";
import { api } from "./config.json";

const getCairoApiInstance = (cairoAuthToken) =>
  axios.create({ headers: { Authorization: `Bearer ${cairoAuthToken}` } });

class Stash extends React.Component {
  constructor(props) {
    super(props);
    const urlSearchParams = new URLSearchParams(window.location.search);
    const params = Object.fromEntries(urlSearchParams.entries());
    this.state = { cairoApi: null };
    if (params.cairoAuthToken) this.setLocalToken(params.cairoAuthToken);
  }

  componentDidMount() {
    this.verifyLocalToken();
  }

  verifyLocalToken() {
    const token = localStorage.getItem("cairoAuthToken");
    axios
      .get(api.cairo.urls.verify, {
        headers: { authorization: `Bearer ${token}` },
      })
      .then(() => this.setState({ cairoApi: getCairoApiInstance(token) }))
      .catch((error) => {
        if (error.response && error.response.status === 401)
          return window.location.replace(
            `${process.env.REACT_APP_CAIRO_URL}/cairo/auth?redirectUri=${window.location.href}`
          );
        console.error("Auth server not up/configured properly!");
      });
  }

  setLocalToken(cairoAuthToken) {
    localStorage.setItem("cairoAuthToken", cairoAuthToken);
    window.history.replaceState({}, document.title, "/");
  }

  render() {
    return (
      <>
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick={true}
          rtl={false}
          limit={3}
          pauseOnFocusLoss
          draggable
          pauseOnHover={false}
        />
        {this.state.cairoApi && <StashBoard cairoApi={this.state.cairoApi} />}
      </>
    );
  }
}

export default Stash;
