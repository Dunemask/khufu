import React from "react";
import ReactDOM from "react-dom";
import Stash from "./Stash";

ReactDOM.render(
  <React.StrictMode>
    <Stash />
  </React.StrictMode>,
  document.getElementById("root")
);
