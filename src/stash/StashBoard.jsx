// Module Imports
import React from "react";
import { toast } from "react-toastify";
// Local Imports
import Stashbar from "./Stashbar";
import StashUpload from "./StashUpload";
import StashContextMenu from "./StashContextMenu";
import { api } from "../config.json";
import "./scss/Stash.scss";
// Constants
const buildFilebox = (file, index) => ({
  file,
  selected: false,
  filtered: true,
  position: index,
});
const buildDownload = (name, total) => ({
  name,
  total,
  completed: 0.01,
  progress: 0.01,
});

class StashBoard extends React.Component {
  constructor(props) {
    super(props);
    window.cairoApi = props.cairoApi;
    this.state = {
      fileBoxes: {},
      downloads: {},
      contextMenu: null,
    };
  }

  componentDidMount() {
    window.cairoApi
      .get(api.nubian.urls.files)
      .then((res) => {
        if (res.data === undefined || res.data.length === undefined)
          return toast.error("Error Loading Files");
        var fileBoxes = {};
        res.data.forEach((file, index) => {
          fileBoxes[file.id] = buildFilebox(file, index);
        });
        this.setState({ fileBoxes });
      })
      .catch((error) => {
        if (error.response.status === 401) return toast.error("Unauthorized!");
        else console.error(error);
      });
  }

  fileBoxesChanged = (fileBoxes) => this.setState({ fileBoxes });

  getSelectedBoxes() {
    var selectedBoxes = [];
    for (var f in this.state.fileBoxes) {
      if (!this.state.fileBoxes[f].filtered) continue;
      if (!this.state.fileBoxes[f].selected) continue;
      selectedBoxes.push(f);
    }
    return selectedBoxes;
  }

  addFilebox(file) {
    var fileBoxes = this.state.fileBoxes;
    fileBoxes[file.id] = buildFilebox(file, Object.keys(fileBoxes).length);
    this.setState({ fileBoxes });
  }

  startDownload(name, total) {
    const downloads = this.state.downloads;
    const key = `${Date.now()}-${name}`;
    const download = buildDownload(name, total);
    download.toast = toast.dark(name, {
      progress: download.progress,
      position: "bottom-right",
    });
    downloads[key] = download;
    this.setState({ downloads });
    return key;
  }

  updateDownload(downloadId, completed, total) {
    const downloads = this.state.downloads;
    const progress = completed / total;
    toast.update(downloads[downloadId].toast, { progress });
  }

  removeDriveContextMenu() {
    if (this.state.contextMenu !== null) this.setState({ contextMenu: null });
  }

  /*Options Menu Functions*/
  contextMenu(e) {
    this.removeDriveContextMenu();
    if (e.ctrlKey || e.shiftKey) return;
    e.preventDefault();
    e.stopPropagation();
    this.setState({
      contextMenu: {
        x: e.clientX,
        y: e.clientY,
      },
    });
  }
  render() {
    return (
      <div
        className="dunestash"
        onClick={this.removeDriveContextMenu.bind(this)}
      >
        <Stashbar
          fileBoxes={this.state.fileBoxes}
          fileBoxesChanged={this.fileBoxesChanged.bind(this)}
          contextMenu={this.contextMenu.bind(this)}
        />
        {this.state.contextMenu && (
          <StashContextMenu
            x={this.state.contextMenu.x}
            y={this.state.contextMenu.y}
            fileBoxes={this.state.fileBoxes}
            fileBoxesChanged={this.fileBoxesChanged.bind(this)}
            getSelectedBoxes={this.getSelectedBoxes.bind(this)}
            startDownload={this.startDownload.bind(this)}
            updateDownload={this.updateDownload.bind(this)}
          />
        )}
        <div className="stash">
          <StashUpload
            addFilebox={this.addFilebox.bind(this)}
            fileBoxes={this.state.fileBoxes}
            fileBoxesChanged={this.fileBoxesChanged.bind(this)}
            contextMenu={this.contextMenu.bind(this)}
            removeDriveContextMenu={this.removeDriveContextMenu.bind(this)}
            getSelectedBoxes={this.getSelectedBoxes.bind(this)}
          />
        </div>
      </div>
    );
  }
}

export default StashBoard;
