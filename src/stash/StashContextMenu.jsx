import React from "react";
import { toast } from "react-toastify";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import FileDownload from "js-file-download";
import {
  faInfoCircle,
  faFileDownload,
  faTrash,
  faEye,
  faShareSquare,
} from "@fortawesome/free-solid-svg-icons";
//Local Imports
import "./scss/stash/StashContextMenu.scss";
import { api } from "../config.json";
//Constants
const downloadUrl = api.nubian.urls.download;
const deleteUrl = api.nubian.urls.delete;
const publicUrl = api.nubian.urls.public;

export default class StashContextMenu extends React.Component {
  infoView() {
    var selectedCount = this.props.getSelectedBoxes().length;
    if (selectedCount > 0) return `${selectedCount} files selected`;
    return "No Files Selected";
  }

  downloadClick() {
    const selectedBoxes = this.props.getSelectedBoxes();
    //ZIPS ARE NOT SUPPORTED YET
    if (selectedBoxes.length > 1)
      return toast.error("Downloading multiple files is not yet supported!");
    return this.handleDownload(
      `${downloadUrl}?target=${selectedBoxes[0]}`,
      this.props.fileBoxes[selectedBoxes[0]].file
    );
  }
  deleteClick() {
    const selectedBoxes = this.props.getSelectedBoxes();
    window.cairoApi
      .post(deleteUrl, selectedBoxes)
      .then((res) => this.handleDelete(res, selectedBoxes))
      .catch((e) => this.handleDelete(e.response, selectedBoxes));
  }
  publicClick() {
    const selectedBoxes = this.props.getSelectedBoxes();
    window.cairoApi
      .post(publicUrl, selectedBoxes)
      .then((res) => this.handlePublic(res, selectedBoxes))
      .catch((e) => this.handlePublic(e.response, selectedBoxes));
  }

  handlePublic(res, selectedBoxes) {
    const failedFiles = res.data || [];
    if (res.status !== 200)
      toast.error("There was an issue making some files public!");
    let fileBoxes = this.props.fileBoxes;
    selectedBoxes.forEach((selectedBoxId) => {
      if (failedFiles.includes(selectedBoxId))
        fileBoxes[selectedBoxId].selected = true;
      else
        fileBoxes[selectedBoxId].file.public = !fileBoxes[selectedBoxId].file
          .public;
    });
    this.props.fileBoxesChanged(fileBoxes);
  }
  handleDownload(url, file) {
    const downloadName = `${file.filename}`;
    const config = {
      method: "GET",
      "Content-Type": "application/octet-stream",
      responseType: "blob",
      onDownloadProgress: (e) =>
        this.props.updateDownload(downloadId, e.loaded, e.total),
    };
    const downloadId = this.props.startDownload(file.filename);
    window.cairoApi.request(url, config).then((response) => {
      if (response.status !== 200)
        return toast.error("There was an error downloading that!");
      FileDownload(response.data, downloadName);
    });
  }
  /**
   * Handles the response from the deleteClick()
   * @param {String} response server response
   * @param {Array} selectedBoxes Selected Boxes object list
   *
   */
  handleDelete(res, selectedBoxes) {
    const failedFiles = res.data || [];
    if (res.status !== 200) toast.error("Error Deleting Some Files");
    let fileBoxes = this.props.fileBoxes;
    selectedBoxes.forEach((selectedBoxId) => {
      if (!failedFiles.includes(selectedBoxId)) delete fileBoxes[selectedBoxId];
      else fileBoxes[selectedBoxId].selected = true;
    });
    this.props.fileBoxesChanged(fileBoxes);
  }
  shareClick() {
    const fileBoxes = this.props.fileBoxes;
    const selectedBoxes = this.props.getSelectedBoxes();
    if (selectedBoxes.length !== 1)
      return toast.error("Only one file can be selected!");
    if (window.location.protocol !== "https:")
      return toast.error("Cannot clipboard without https!");
    const url = `${window.location.origin}/${downloadUrl}?target=${selectedBoxes[0]}`;
    navigator.clipboard.writeText(url);
    toast.success("Link successfully copied!");
  }

  styleCalc() {
    const estimatedHeight = 180; //px
    const esetimatedWidth = 290; //px
    const bodyWidth = document.body.offsetWidth;
    const bodyHeight = document.documentElement.offsetHeight;
    let top = this.props.y;
    let left = this.props.x;
    const overFlowX = left + esetimatedWidth > bodyWidth;
    const overFlowY = top + estimatedHeight > bodyHeight;
    if (overFlowX) left = left - esetimatedWidth;
    if (overFlowY) top = top - estimatedHeight;
    return { top: `${top}px`, left: `${left}px` };
  }

  render() {
    return (
      <div className="drive-context-menu" style={this.styleCalc()}>
        <ul>
          <li>
            <FontAwesomeIcon icon={faInfoCircle} />
            {this.infoView()}
          </li>
          <li onClick={this.downloadClick.bind(this)}>
            <FontAwesomeIcon icon={faFileDownload} />
            Download
          </li>
          <li onClick={this.deleteClick.bind(this)}>
            <FontAwesomeIcon icon={faTrash} />
            Delete
          </li>
          <li onClick={this.publicClick.bind(this)}>
            <FontAwesomeIcon icon={faEye} />
            Toggle Public
          </li>
          <li onClick={this.shareClick.bind(this)}>
            <FontAwesomeIcon icon={faShareSquare} />
            Share
          </li>
        </ul>
      </div>
    );
  }
}
