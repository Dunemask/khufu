FROM node:16
WORKDIR /dunemask/net/khufu
COPY package.json .
RUN npm i
COPY public public
COPY src src
ARG REACT_APP_CAIRO_URL
RUN npm run build:react
COPY server server
CMD ["npm", "start"]
